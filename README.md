# AML (Adaptive Multi-modal bug Localization)

### Information Retrieval and Spectrum Based Bug Localization: Better Together [[pdf]](https://sites.google.com/site/lebuitienduy/fse2015.pdf?attredirects=0&d=1)

## Dataset & Tool Release

AML's data & tool [[7z]](https://drive.google.com/file/d/0B4yUCAFGLAe2RXl0ZXplNW54UWs/view?usp=sharing)

Bug List & Recovered links between bug reports and commits [[7z]](https://drive.google.com/file/d/0B4yUCAFGLAe2eU9MUUt3Vl9fUkE/view?usp=sharing)

**Pre-fix and post-fix versions:**

* Ant [[.tar.bz2 ~2GB]](https://drive.google.com/file/d/0B4yUCAFGLAe2QmlqaC1zb21mMW8/view?usp=sharing)

* Lucene [[.tar.bz2 ~5GB]](https://drive.google.com/file/d/0B4yUCAFGLAe2dWppWWR2TnJHa1k/view?usp=sharing)

* Rhino [[.tar.bz2 ~135MB]](https://drive.google.com/file/d/0B4yUCAFGLAe2anVMbVVFZndvbGc/view?usp=sharing)

* AspectJ [[iBugs]](https://www.st.cs.uni-saarland.de/ibugs/ibugs_aspectj-1.3.tar.gz)  

## Bibtex
```
@inproceedings{aml,
  title={Information retrieval and spectrum based bug localization: Better together},
  author={Le, Tien-Duy and Oentaryo, Richard J and Lo, David},
  booktitle={Proceedings of the 2015 10th Joint Meeting on Foundations of Software Engineering},
  pages={579--590},
  year={2015},
  organization={ACM}
}
```

## [New Github repository]: https://github.com/lebuitienduy/aml